# Spring Security Demo

### Authentication with DB IdP  
`https://localhost:8443/login`

**Users**  
user/user => ROLE_USER  
admin/admin => ROLE_ADMIN  

### Role Based Authorization  

Authenticated users  
`https://localhost:8443/open`

ROLE_ADMIN or ROLE_SUPER  
`https://localhost:8443/adminorsuper`

ROLE_SUPER  
`https://localhost:8443/superonly`

### Attributes Control List (ACL) - domain object security

Create sample ACLPermissions:  

Read (Allowed for user & admin)  
`https://localhost:8443/get/1`

Write (Allowed for user)  
`https://localhost:8443/update/1`

### SSO with Facebook

Register your application in Facebook
<https://developers.facebook.com/quickstarts/>

App Domain:  
`localhost`  

Web site URL:  
`https://localhost:8443/`

Valid OAuth redirect URIs  
`https://localhost:8443/auth/facebook`  

Enter your appId and appSecret in application.properties (or as vm argument)