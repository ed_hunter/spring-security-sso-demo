package com.example.ssodemo.dbauth.persistence;

import com.example.ssodemo.dbauth.Role;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.social.security.SocialUserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Entity
@Data
public class User implements SocialUserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private boolean enabled;

    @Column(nullable = false)
    private String role;

    @Column
    private String providerID;

    @Transient
    private boolean accountNonExpired;

    @Transient
    private boolean accountNonLocked;

    @Transient
    private boolean credentialsNonExpired;

    @Transient
    private List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

    public User() {
        this.accountNonExpired = true;
        this.accountNonLocked = true;
        this.credentialsNonExpired = true;
    }

    public User(String username, String password, Role role) {
        this.username = username;
        this.password = password;
        this.role = role.getValue();
        this.enabled = true;
    }

    // Authorites aus String Role aufbauen
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (authorities.isEmpty()) {
            populateAuthoritiesFromRole();
        }
        return authorities;
    }

    private void populateAuthoritiesFromRole() {
        this.authorities.clear();
        this.authorities.add(new SimpleGrantedAuthority(role));
    }

    @Override
    public String getUserId() {
        return String.valueOf(id);
    }
}