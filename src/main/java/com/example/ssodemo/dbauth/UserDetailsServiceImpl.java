package com.example.ssodemo.dbauth;

import com.example.ssodemo.dbauth.persistence.User;
import com.example.ssodemo.dbauth.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.social.security.SocialUserDetailsService;

import java.util.Optional;

public class UserDetailsServiceImpl implements UserDetailsService, SocialUserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDetails user = userRepository.findByUsername( username );
        if(user == null) {
            throw new UsernameNotFoundException(username);
        }
        return user;
    }

    @Override
    public SocialUserDetails loadUserByUserId(String id) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findById( Long.parseLong(id) );
        if(!user.isPresent()) {
            throw new UsernameNotFoundException(id);
        }
        return user.get();
    }
}
