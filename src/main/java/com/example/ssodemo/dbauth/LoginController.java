package com.example.ssodemo.dbauth;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {
    @RequestMapping("/login")
    public ModelAndView showLoginPage() {
        // Path templates/login/login.html
         return new ModelAndView("login/login");
    }
}
