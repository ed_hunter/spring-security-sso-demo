package com.example.ssodemo.dbauth;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SecuredController {

    @RequestMapping("/superonly")
    @PreAuthorize("hasRole('ROLE_SUPER')")
    @ResponseBody
    String adminonly() {
        return "Only super";
    }

    @RequestMapping("/adminorsuper")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER')")
    @ResponseBody String adminorsuper() {
        return "Admin or Super";
    }

    @RequestMapping("/open")
    @PreAuthorize("isAuthenticated()")
    @ResponseBody String open() {
        return "Authenticated Users";
    }
}