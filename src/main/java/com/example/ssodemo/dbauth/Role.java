package com.example.ssodemo.dbauth;

import lombok.Getter;

public enum Role {
    ROLE_USER("ROLE_USER"), ROLE_ADMIN("ROLE_ADMIN"), ROLE_SUPER("ROLE_SUPER");

    @Getter
    private String value;

    private Role(String value) {
        this.value = value;
    }
}