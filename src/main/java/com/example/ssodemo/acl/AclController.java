package com.example.ssodemo.acl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AclController {

    @Autowired
    AclSupport aclSupport;

    @RequestMapping("/get/{id}")
    @PreAuthorize("hasPermission(#id, 'BlogPost', 'READ')")
    @ResponseBody
    String get(@PathVariable Long id) {
        return id + " can be viewed";
    }

    @RequestMapping("/update/{id}")
    @PreAuthorize("hasPermission(#id, 'BlogPost', 'WRITE')")
    @ResponseBody String fakepost(@PathVariable Long id) {
        return id + " can be updated";
    }

    @RequestMapping("/create")
    @PreAuthorize("isAuthenticated()")
    @ResponseBody String createAcl() {
        aclSupport.grantReadPermission("BlogPost", 1l, "user");
        aclSupport.grantWritePermission("BlogPost", 1l, "user");
        aclSupport.grantReadPermission("BlogPost", 1l, "admin");
        return "Acl's created.";
    }
}