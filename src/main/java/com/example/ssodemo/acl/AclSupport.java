package com.example.ssodemo.acl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.domain.ObjectIdentityImpl;
import org.springframework.security.acls.domain.PrincipalSid;
import org.springframework.security.acls.model.*;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

@Component
@Transactional
public class AclSupport {
    @Autowired
    private MutableAclService aclService;

    @Autowired
    private PermissionGrantingStrategy permissionGrantingStrategy;

    public void grantReadPermission(String type, Long id, String username) {
        grantPermission(type, id, username, BasePermission.READ);
    }

    public void grantWritePermission(String type, Long id, String username) {
        grantPermission(type, id, username, BasePermission.WRITE);
    }

    private void grantPermission(String type, Long id, String username, Permission permission) {
        MutableAcl acl = getOrCreateAcl(type, id);
        acl.insertAce(acl.getEntries().size(), permission, new PrincipalSid(username), true);
        aclService.updateAcl(acl);
    }

    public boolean canRead(String type, Long id, String username) {
        return hasPermission(type, id, username, BasePermission.READ);
    }

    public boolean canWrite(String type, Long id, String username) {
        return hasPermission(type, id, username, BasePermission.WRITE);
    }

    private boolean hasPermission(String type, Long id, String username, Permission permission) {
        try {
            Acl acl = aclService.readAclById(new ObjectIdentityImpl(type, id));
            if (acl != null) {
                return permissionGrantingStrategy.isGranted(acl, Arrays.asList(permission), Arrays.asList(new PrincipalSid(username)), true);
            } else {
                return false;
            }
        } catch (NotFoundException nfe) {
            return false;
        }
    }

    private MutableAcl getOrCreateAcl(String type, Long identifier) {
        MutableAcl acl = readAcl(type, identifier);
        if (acl == null) {
            acl = aclService.createAcl(new ObjectIdentityImpl(type, identifier) );
        }
        return acl;
    }

    private MutableAcl readAcl(String type, Long identifier) {
        ObjectIdentity oi = new ObjectIdentityImpl(type, identifier);
        try {
            return (MutableAcl) aclService.readAclById(oi);
        } catch (NotFoundException nfe) {
            return null;
        }
    }
}