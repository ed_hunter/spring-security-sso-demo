package com.example.ssodemo;

import com.example.ssodemo.dbauth.Role;
import com.example.ssodemo.dbauth.persistence.User;
import com.example.ssodemo.dbauth.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class SsoDemoApplication implements CommandLineRunner {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(SsoDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		createUserIfNotExisits("user", "user", Role.ROLE_USER);
		createUserIfNotExisits("admin", "admin", Role.ROLE_ADMIN);
	}

	private void createUserIfNotExisits(String username, String pwd, Role role) {
		try {
			userRepository.save(
					new User(username, passwordEncoder.encode(pwd), role));
		} catch(Exception e) {
			// User gibt es schon
		}
	}
}
