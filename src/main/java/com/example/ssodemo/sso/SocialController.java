package com.example.ssodemo.sso;

import com.example.ssodemo.dbauth.Role;
import com.example.ssodemo.dbauth.persistence.User;
import com.example.ssodemo.dbauth.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

@Controller
public class SocialController {

    @Autowired
    ProviderSignInUtils providerSignInUtils;

    @Autowired
    UserRepository userRepository;

    @RequestMapping("/socialwelcome")
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    String landingpage() {
        return "SSO login successful";
    }

    @RequestMapping("/signup")
    public @ResponseBody String doRegister(WebRequest request) {

        Connection connection = providerSignInUtils.getConnectionFromSession(request);
        if(connection == null) {
            return "No Facebook connection...";
        }
        Facebook facebook = (Facebook) connection.getApi();
        String [] fields = { "id", "email",  "first_name", "last_name" };
        org.springframework.social.facebook.api.User userProfile = facebook.fetchObject("me", org.springframework.social.facebook.api.User.class, fields);
        User user = new User();
        user.setUsername(userProfile.getFirstName() + "_" + userProfile.getLastName());
        user.setPassword("make password nullable");
        user.setRole(Role.ROLE_USER.getValue());

        // connection Facebook-Provider
        user.setProviderID(connection.getKey().getProviderUserId());
        // flush to be sure that id is created
        userRepository.saveAndFlush(user);
        // ma
        providerSignInUtils.doPostSignUp("" + user.getId(), request);

        return "Facebook User created: " + user.getUsername();
    }
}