package com.example.ssodemo.sso;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.UserIdSource;
import org.springframework.social.security.SocialUserDetails;

public class ProviderIdBasedUserIdSource implements UserIdSource {
    @Override
    public String getUserId() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        if(principal instanceof SocialUserDetails) {
            return ((SocialUserDetails)principal).getUserId();
        } else {
            return SecurityContextHolder.getContext().getAuthentication().getName();
        }
    }
}
